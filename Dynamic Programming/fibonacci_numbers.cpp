class Solution {
public:
    int fib(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        vector<int> fibs = {0,1};
        int i = 1;
        while (i < n) {
            fibs.push_back(fibs[i] + fibs[i - 1]);
            i++;
        }
        return fibs[fibs.size() - 1];
    }
};
