class Solution {
public:
  vector<int> decrypt(vector<int>& code, int k) {
    vector<int> result;
    if (k == 0) {
      for (int i = 0; i < code.size(); i++) {
        result.push_back(0);
      }
    } else if (k < 0) {
      // previous
      int end = code.size() - 1;
      int next_t = 0;
      for (int i = 0; i < abs(k); i++) {
        code.insert(code.begin(), code[end]);
      }
      for (int i = 1; i <= abs(k); i++) {
        next_t += code[code.size() - i - 1];
      }
      result.insert(result.begin(), next_t);
      for (int i = (code.size() - 2); i >= abs(k); i--) {
        next_t += code[i - abs(k)] - code[i];
        result.insert(result.begin(), next_t);
      }
    } else {
      // next
      int next_t = 0;
      for (int i = 0; i < k; i++) {
        code.push_back(code[i]);
      }
      for (int i = 1; i < k + 1; i++) {
        next_t += code[i];
      }
      result.push_back(next_t);
      for (int i = 1; i < code.size() - k; i++) {
        next_t += code[i + k] - code[i];
        result.push_back(next_t);
      }

    }
    return result;
  }
};
