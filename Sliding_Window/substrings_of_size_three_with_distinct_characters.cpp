class Solution {
public:

    bool disctinct_three(char a, char b, char c) {
        return a != b && a != c && b != c;
    }

    int countGoodSubstrings(string s) {
        int subs = 0;
        for (int i = 2; i < s.size(); i++) {
            if (disctinct_three(s[i-2], s[i-1], s[i])) {
                subs++;
            }
        }
        return subs;
    }
};
