class Solution {
public:
    int maximumStrongPairXor(vector<int>& nums) {
        int max_xor = 0;
        for (int x = 0; x < nums.size(); x++) {
            for (int y = x; y < nums.size(); y++) {
                if (abs(nums[x] - nums[y]) <= min(nums[x],nums[y])) {
                    max_xor = max(max_xor, nums[x]^nums[y]);
                }
            }
        }
        return max_xor;
    }
};
