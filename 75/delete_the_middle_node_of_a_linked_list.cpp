/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* deleteMiddle(ListNode* head) {
        int n = 0;
        ListNode* h = head;
        while (h) {
            h = h -> next;
            n++;
        }
        n = n/2;
        h = head;
        if (n == 0) {
            return nullptr;
        }
        while (n-- > 1) {
            h = h -> next;
        }
        ListNode* next = h -> next -> next;
        h -> next = next;
        return head;
    }
};
