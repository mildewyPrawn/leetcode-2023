#include <bits/stdc++.h>
using namespace std;

bool is_vowel(char c) {
  return c == 'a' || c == 'A' || c == 'e' || c == 'E' ||
    c == 'i' || c == 'I' || c == 'o' || c == 'O' || c == 'u' || c == 'U';
}

string swap_vowels(string s) {
  int i = 0;
  int j = s.length();
  while (i < j) {
    while (!is_vowel(s[i]) && i < j) {
      i++;
    }
    while (!is_vowel(s[j]) && j > i) {
      j--;
    }
    swap(s[i], s[j]);
    i++;
    j--;
  }
  return s;
}

/*
Unglad, I tar a tidal gnu.
i                       j
 */

int main() {
  string s = "Unglad, I tar a tidal gnu.";

  string rev_s = swap_vowels(s);
  cout << rev_s << "\n";
}
