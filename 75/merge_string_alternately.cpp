class Solution {
public:
    string mergeAlternately(string word1, string word2) {
        int i = word1.length();
        int j = word2.length();
        int k = 0;
        string result = "";
        while (k < i && k < j) {
            result = result + word1.at(k) + word2.at(k);
            k++;
        }
        while (k < i) {
            result += word1.at(k);
            k++;
        }
        while (k < j) {
            result += word2.at(k);
            k++;
        }
        return result;
    }
};
