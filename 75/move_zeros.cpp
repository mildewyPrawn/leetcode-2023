#include <vector>

class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        vector<int> prev;
        for (int i : nums) {
            if (i == 0) {
                continue;
            } else {
                prev.push_back(i);
            }
        }
        int i = 0;
        while (i < prev.size()) {
            nums[i] = prev[i];
            i++;
        }
        while (i < nums.size()) {
            nums[i] = 0;
            i++;
        }
    }
};
