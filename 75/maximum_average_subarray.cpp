class Solution {
public:
    double findMaxAverage(vector<int>& nums, int k) {
        double max_avg_sba = 0;
        for (int i = 0; i < k; i++) {
            max_avg_sba += nums[i]/(double)k;
        }
        double window_sum = max_avg_sba;
        for (int i = k; i < nums.size(); i++) {
            window_sum += (nums[i]/(double)k) - (nums[i - k]/(double)k);
            max_avg_sba = max(max_avg_sba, window_sum);
        }
        return max_avg_sba;
    }
};
