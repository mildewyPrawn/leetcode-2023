class Solution {
public:

    bool is_vowel(char c) {
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
    }

    int maxVowels(string s, int k) {
        int max_vowels = 0;
        for (int i = 0; i < k; i++) {
            if (is_vowel(s[i])) {
                max_vowels++;
            }
        }
        int window_vowels = max_vowels;
        for (int i = k; i < s.size(); i++) {
            window_vowels += is_vowel(s[i]) ? 1 : 0;
            window_vowels += is_vowel(s[i - k]) ? -1 : 0;
            max_vowels = max(max_vowels, window_vowels);
        }
        return max_vowels;
    }
};
