#include <bits/stdc++.h>
using namespace std;

int pivotIndex(vector<int>& nums) {
  int res = 0, r_sum = 0, l_sum = 0, i = 0;
  for (int i : nums) {
    r_sum += i;
  }
  while (i < nums.size()) {
    r_sum = r_sum - nums[i];
    cout << "I: " << i << " L: " << l_sum << " R: " << r_sum << "\n";
    if (l_sum == r_sum) {
      return i;
    }
    l_sum = l_sum + nums[i++];
  }
  return -1;
}

int main() {
  vector<int> nums = {1,7,3,6,5,6};
  // vector<int> nums = {1,2,3};
  // vector<int> nums = {2,1,-1};
  // vector<int> nums = {2,5};
  int p = pivotIndex(nums);
  cout << p << "\n";
}
