/**
 * Forward declaration of guess API.
 * @param  num   your guess
 * @return 	     -1 if num is higher than the picked number
 *			      1 if num is lower than the picked number
 *               otherwise return 0
 * int guess(int num);
 */

class Solution {
public:
    int guessNumber(int n) {
        int low = 1, high = n;
        int res = 1;
        while (low <= high) {
            int m = low + (high - low) / 2;
            int g_num = guess(m);
            if (g_num == 0) {
                res = m;
                break;
            } else if (g_num > 0) { // lower
                low = m + 1;
            } else { // higher
                high = m - 1;
            }
        }
        return res;
    }
};
