class Solution {
public:
    vector<bool> kidsWithCandies(vector<int>& candies, int extraCandies) {
        int max = 0, i = 0;
        for (int i : candies) {
            if (i > max) {
                max = i;
            }
        }
        vector<bool> res;
        while (i < candies.size()) {
            if (candies[i] + extraCandies >= max) {
                res.push_back(true);
            } else {
                res.push_back(false);
            }
            i++;
        }
        return res;
    }
};
