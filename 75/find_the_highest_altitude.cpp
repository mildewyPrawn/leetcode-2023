#include <bits/stdc++.h>
using namespace std;

int largestAltitude(vector<int>& gain) {
  int res = 0, i = 0, curr = 0;
  while (i < gain.size()) {
    int alt = curr + gain[i++];
    curr = alt;
    res = alt > res ? alt : res;
  }
  return res;
}

int main() {
  vector<int> v = {-5, 1, 5, 0, -7};
  /*
    i = 0; res: 0; alt = 0 + (-5); curr = 0 -> -5 > 0 = 0
    i = 1; res: 0; alt = -5 + 1; curr = -5  -> -4 > 0 = 0
    i = 2; res: 0; alt =  + 5; curr = 5
  */
  int i = largestAltitude(v);
  cout << i << "\n";
}
