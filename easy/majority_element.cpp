class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int max = nums[0];
        for (int n : nums) {
            if (n > max) {
                max = n;
            }
        }
        return max;
    }
};
