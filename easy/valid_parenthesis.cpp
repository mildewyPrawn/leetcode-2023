class Solution {
public:
    bool isValid(string s) {
        stack<char> q;
        for (char c : s) {
            if (c == '(' || c == '[' || c == '{')
                q.push(c);
            if (q.size() == 0)
                return false;
            if (c == ')' && q.size() > 0)
                if (q.top() == '(')
                    q.pop();
                else
                    return false;
            if (c == ']' && q.size() > 0)
                if (q.top() == '[')
                    q.pop();
                else
                    return false;
            if (c == '}' && q.size() > 0)
                if (q.top() == '{')
                    q.pop();
                else
                    return false;
        }
        cout << q.size() << endl;
        return q.size() == 0;
    }
};
